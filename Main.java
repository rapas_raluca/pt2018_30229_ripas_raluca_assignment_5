import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

    public static void main (String[] args){
        List<MonitoredData> data;
        data=citireFisier();
        numberOfDistinctDays(data);
        numberOfActivities(data);
        numberOfActivitiesForEachDay(data);
        //for(MonitoredData m: data){
          //  System.out.println(m.toString());
        //}
        tenHoursActivities(data);
    }

    private static List<MonitoredData> citireFisier(){
        try {
            return Files.lines(Paths.get("E:\\an2\\t5\\Activities.txt")).map((t)->{
                String[] str= t.split("\t\t");
                LocalDateTime start=LocalDateTime.parse(str[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                LocalDateTime end=LocalDateTime.parse(str[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                MonitoredData m=new MonitoredData(start,end,str[2]);
                System.out.println(m.toString());
                return m;
            }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

   /* public static List<MonitoredData> citireFisier1(){
        try {
            return Files.lines(Paths.get("E:/an2/t5/Activities.txt")).map((t)->new MonitoredData(t)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/

   private static void numberOfDistinctDays(List<MonitoredData> data){
       long number=data.stream().map((nr)->nr.getStartTime().getDayOfMonth()).distinct().count();
        System.out.println("There are "+number+" distinct days");
   }

   private static void numberOfActivities(List<MonitoredData> data){
        Map<String,Long> act=data.stream().collect(Collectors.groupingBy((n)->n.getActivity(),Collectors.counting()));
        try {
            FileWriter fw = new FileWriter("numberOfActivities.txt", false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter finalOut = new PrintWriter(bw);
            act.forEach((k, v)-> {
                String s=k+"="+v;
                finalOut.println(s);});
            finalOut.println("\n");
            finalOut.close();
            fw.close();
            bw.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
   }

   private  static void numberOfActivitiesForEachDay(List<MonitoredData> data){
        Map<Integer,Map<String,Long>> act=data.stream().collect(Collectors.groupingBy((d)->d.getStartTime().getDayOfMonth(),
                Collectors.groupingBy((a)->a.getActivity(),Collectors.counting())));
       try {
           FileWriter fw = new FileWriter("numberOfActivitiesForEachDay.txt", false);
           BufferedWriter bw = new BufferedWriter(fw);
           PrintWriter finalOut = new PrintWriter(bw);
           act.forEach((day, v)-> {
               String s=day+"-> ";
               finalOut.println(s);
               v.forEach((k,value)->{
                   String s1=k+" = "+value;
                  finalOut.println(s1);
               });
           });
           finalOut.println("\n");
           finalOut.close();
           fw.close();
           bw.close();
       } catch (IOException e) {
           System.out.println(e.getMessage());
       }
   }

   private static void tenHoursActivities(List<MonitoredData> data){
       Map<String,Long> dur=data.stream().collect(Collectors.groupingBy((n)->n.getActivity(),Collectors.summingLong((n)->n.getSecondsBetween())));
       try {
           FileWriter fw = new FileWriter("Activities10hours.txt", false);
           BufferedWriter bw = new BufferedWriter(fw);
           PrintWriter finalOut = new PrintWriter(bw);
           dur.entrySet().stream().filter((s) -> s.getValue() >= 36000).forEach((n) -> {
               int hours = n.getValue().intValue() / 3600;
               int remainder = n.getValue().intValue() - hours * 3600;
               int mins = remainder / 60;
               remainder = remainder - mins * 60;
               int secs = remainder;
               finalOut.println(n.getKey() + " - " + hours + " hours " + mins + " minutes " + secs+" seconds");
           });
           finalOut.println("\n");
           finalOut.close();
           fw.close();
           bw.close();
       } catch (IOException e1) {
           e1.printStackTrace();
       }
   }

   /* private double percent(int total,int x){
       return (x*100.0)/total;
    }
       private static void fiveMinutesActivities(List<MonitoredData> data){
          List<String> acc=data.stream().collect(Collectors.groupingBy((n)->n.getActivity(),Collectors.counting()));
           Map<String,Long> nrTotal=data.stream().collect(Collectors.groupingBy((n)->n.getActivity(),Collectors.counting()));
           Map<String,Long> fiveMinutes=data.stream().collect(Collectors.groupingBy((n)->n.getActivity(),Collectors.counting()));
           fiveMinutes.entrySet().stream().filter((s)->{

                   });
           )
       }*/
}
