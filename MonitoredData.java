import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class MonitoredData {

    private String activity;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    public MonitoredData(LocalDateTime startTime,LocalDateTime endTime, String activity){
        this.activity=activity;
        this.startTime=startTime;
        this.endTime=endTime;
    }

    @Override
    public String toString() {
        return "MonitoredData{ " +
                "activity='" + activity + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
//.map.colect
    // de la linia de str la monitored data
    //joda

    public String getActivity() {
        return activity;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }



    public Long getSecondsBetween(){
        Duration d=Duration.between(startTime,endTime);
        long seconds=d.getSeconds();
        return seconds;
    }
}
